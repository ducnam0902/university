package university.client.Controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import university.client.Model.Faculty;

@Controller
@RequestMapping("/faculty")
public class FacultyController {
	private RestTemplate rest = new RestTemplate();

	@GetMapping
	public String showFaculty(Model model) {
		List<Faculty> Faculty = Arrays.asList(rest.getForObject("http://localhost:8080/faculty", Faculty[].class));
		model.addAttribute("Faculty", Faculty);
		return "Faculty/Faculty";
	}

	@GetMapping("/add")
	public String addNewFaculty() {
		return "Faculty/addNewFaculty";
	}

	@PostMapping("/add")
	public String storeFaculty(@RequestParam("name") String name, @RequestParam("add") String add,
			@RequestParam("phone") String phone, Model model) {

		// Create a faculty object to store data from form
		Faculty fa = new Faculty(null, name, add, phone);

		// Check Faculty and Name 's duplicate to another record
		Boolean checkFacultyInsert = rest.postForObject("http://localhost:8080/faculty/checkFacultyInsert", fa,
				Boolean.class);

		if (checkFacultyInsert) {
			rest.postForObject("http://localhost:8080/faculty", fa, Faculty.class);
			return "redirect:/faculty";
		} else {
			String message = "Faculty name or Phone is existed. Please check again!";
			model.addAttribute("faculty", fa);
			model.addAttribute("message", message);
			return "Faculty/addNewFaculty";
		}

	}

	@GetMapping("/edit/{id}")
	public String editFaculty(@PathVariable("id") String id, Model model) {
		Faculty faculty = rest.getForObject("http://localhost:8080/faculty/find/{id}", Faculty.class, id);
		model.addAttribute("faculty", faculty);
		return "Faculty/updateFaculty";
	}

	@PostMapping("/edit/{id}")
	public String updateFaculty(@PathVariable("id") String id,@RequestParam("name") String name, @RequestParam("add") String add,
			@RequestParam("phone") String phone, Model model) {
		Faculty faculty = new Faculty(id,name,add,phone);
		Boolean checkFacultyUpdate = rest.postForObject("http://localhost:8080/faculty/checkFacultyUpdate",faculty ,Boolean.class);
		if(checkFacultyUpdate) {
			rest.put("http://localhost:8080/faculty/edit", faculty);
			return "redirect:/faculty";
		}
		else {
			model.addAttribute("faculty", faculty);
			String message = "Faculty name or Phone is existed. Please check again!";
			model.addAttribute("message", message);
			return "Faculty/updateFaculty";
		}
	}
	@GetMapping("/search")
	public String searchByKeyword(@RequestParam("keyword") String keyword,Model model){
		List<Faculty> faculty= Arrays.asList(rest.getForObject("http://localhost:8080/faculty/search/{keyword}", Faculty[].class,keyword));
		model.addAttribute("Faculty", faculty);
		return "Faculty/Faculty";
	}
}
