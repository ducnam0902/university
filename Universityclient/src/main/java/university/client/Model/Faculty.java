package university.client.Model;
public class Faculty {
	private String FacultyId;
	private String FacultyName;
	private String address;
	private String phone;
	public Faculty(String FacultyId, String FacultyName, String address, String phone) {
		super();
		this.FacultyId = FacultyId;
		this.FacultyName = FacultyName;
		this.address = address;
		this.phone = phone;
	}
	public Faculty() {
		super();
	}
	public String getFacultyId() {
		return FacultyId;
	}
	public void setFacultyId(String FacultyId) {
		this.FacultyId = FacultyId;
	}
	public String getFacultyName() {
		return FacultyName;
	}
	public void setFacultyName(String FacultyName) {
		this.FacultyName = FacultyName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
}
