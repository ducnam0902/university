package university.client.Model;



public class Major {

	private String majorId;
	private String majorName;
	private Faculty faculty;
	public Major(String majorId, String majorName, Faculty faculty) {
		super();
		this.majorId = majorId;
		this.majorName = majorName;
		this.faculty = faculty;
	}
	public Major() {
		super();
	}
	public String getMajorId() {
		return majorId;
	}
	public void setMajorId(String majorId) {
		this.majorId = majorId;
	}
	public String getMajorName() {
		return majorName;
	}
	public void setMajorName(String majorName) {
		this.majorName = majorName;
	}
	public Faculty getFaculty() {
		return faculty;
	}
	public void setFalcuty(Faculty faculty) {
		this.faculty = faculty;
	}
	
}
