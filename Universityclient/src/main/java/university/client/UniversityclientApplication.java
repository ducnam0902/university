package university.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UniversityclientApplication {

	public static void main(String[] args) {
		SpringApplication.run(UniversityclientApplication.class, args);
	}

}
