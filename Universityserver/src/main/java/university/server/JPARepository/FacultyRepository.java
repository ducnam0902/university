package university.server.JPARepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import university.server.Model.Faculty;

@Repository
public interface FacultyRepository extends JpaRepository<Faculty, String> {
	
	@Query(value ="SELECT DISTINCT FACULTY_NAME FROM FACULTY WHERE FACULTY_NAME=?1",nativeQuery=true)
	public String checkNameFaculty(String falcutyName);
	
	@Query(value ="SELECT DISTINCT PHONE FROM FACULTY WHERE PHONE=?1",nativeQuery=true)
	public String checkPhoneFaculty(String phone);
	
	@Query(value="SELECT CONCAT(REPEAT(0,3-LENGTH(MAX(substring(FACULTY_ID,2,3))+1)),MAX(substring(FACULTY_ID,2,3))+1) FROM FACULTY;",nativeQuery=true)
	public String getMaxId();
	
	@Query(value="SELECT DISTINCT FACULTY_NAME FROM FACULTY WHERE FACULTY_ID <>?1 AND FACULTY_NAME =?2 ", nativeQuery = true)
	public String checkDuplicateNameFaculty(String id, String facultyName);
	
	@Query(value="SELECT DISTINCT PHONE FROM FACULTY WHERE FACULTY_ID <>?1 AND PHONE=?2", nativeQuery = true)
	public String checkDuplicatePhone(String id,String phone);
	
	@Query(value = "SELECT * FROM FACULTY WHERE FACULTY_NAME LIKE %?1% OR ADDRESS LIKE %?1% OR PHONE LIKE %?1%", nativeQuery = true)
	public List<Faculty> FacultySearch(String keyword);
	
}
