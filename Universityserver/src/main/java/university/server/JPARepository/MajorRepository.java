package university.server.JPARepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import university.server.Model.Major;

@Repository
public interface MajorRepository extends JpaRepository<Major,String> {

}
