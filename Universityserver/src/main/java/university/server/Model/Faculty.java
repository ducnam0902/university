package university.server.Model;


import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Faculty {
	@Id
	private String facultyId;
	private String facultyName;
	private String address;
	private String phone;
	
	@OneToMany(mappedBy ="faculty",cascade = CascadeType.ALL)
	private List<Major> major;

	public Faculty(String facultyId, String facultyName, String address, String phone) {
		super();
		this.facultyId = facultyId;
		this.facultyName = facultyName;
		this.address = address;
		this.phone = phone;
	}

	public Faculty() {
		super();
	}

	public String getFacultyId() {
		return facultyId;
	}

	public void setFacultyId(String facultyId) {
		this.facultyId = facultyId;
	}

	public String getFacultyName() {
		return facultyName;
	}

	public void setFacultyName(String facultyName) {
		this.facultyName = facultyName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	
}
