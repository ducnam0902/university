package university.server.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Major {
	@Id
	private String majorId;
	private String majorName;
	private String purpose;
	private int totalYear;

	@ManyToOne
	@JoinColumn(name="faculty_id")
	private Faculty faculty;
	public Major(String majorId, String majorName,String purpose, int totalYear ,Faculty faculty) {
		super();
		this.majorId = majorId;
		this.majorName = majorName;
		this.faculty = faculty;
		this.totalYear= totalYear;
		this.purpose=purpose;
	}
	public Major() {
		super();
	}
	public String getMajorId() {
		return majorId;
	}
	public Faculty getFaculty() {
		return faculty;
	}
	public void setFaculty(Faculty faculty) {
		this.faculty = faculty;
	}
	public void setMajorId(String majorId) {
		this.majorId = majorId;
	}
	public String getPurpose() {
		return purpose;
	}
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	public String getMajorName() {
		return majorName;
	}
	public void setMajorName(String majorName) {
		this.majorName = majorName;
	}
	public int getTotalYear() {
		return totalYear;
	}
	public void setTotalYear(int totalYear) {
		this.totalYear = totalYear;
	}
	
}
