package university.server.RestController;

import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import university.server.Model.Faculty;
import university.server.Services.FacultyServices;

@RestController
@RequestMapping(path = "/faculty", produces = "application/json")
@CrossOrigin(origins = "*")
public class FacultyController {

	private FacultyServices facultyServices;

	public FacultyController(FacultyServices facultyServices) {
		this.facultyServices = facultyServices;
	}

	// Show all Faculty
	@GetMapping
	public List<Faculty> findAllfaculty() {
		return facultyServices.findAll();
	}

	// check and insert faculty
	@PostMapping("/checkFacultyInsert")
	public Boolean checkFalcutyInsert(@RequestBody Faculty faculty) {
		String checkNameFalcuty = facultyServices.checkNameFaculty(faculty.getFacultyName());
		String checkPhoneFalcuty = facultyServices.checkPhoneFaculty(faculty.getPhone());
		if (checkNameFalcuty == null && checkPhoneFalcuty == null) {
			return true;
		} else {
			return false;
		}
	}

	@PostMapping
	public void savefaculty(@RequestBody Faculty faculty) {
		String facultyId;
		if(facultyServices.getMaxId() == null) {
			facultyId ="F001";
		}
		else 
			facultyId= "F" + facultyServices.getMaxId();
		faculty.setFacultyId(facultyId);
		facultyServices.save(faculty);
	}

	// check and update faculty
	@PostMapping("/checkFacultyUpdate")
	public Boolean checkFacultyUpdate(@RequestBody Faculty faculty) {
		String checkDuplicateNameFaculty = facultyServices.checkDuplicateNameFaculty(faculty.getFacultyId(),
				faculty.getFacultyName());
		String checkDuplicatePhone = facultyServices.checkDuplicatePhone(faculty.getFacultyId(), faculty.getPhone());

		if (checkDuplicateNameFaculty == null && checkDuplicatePhone == null) {
			return true;
		} else
			return false;

	}

	@PutMapping("/edit")
	public void updatefaculty(@RequestBody Faculty faculty) {
		facultyServices.save(faculty);
	}

	@DeleteMapping("/delete/{id}")
	public void remove(@PathVariable("id") String id) {
		facultyServices.remove(id);
	}

	@GetMapping("/find/{id}")
	public Optional<Faculty> findById(@PathVariable("id") String id) {
		return facultyServices.findById(id);
	}

	@GetMapping("/findobject/{id}")
	public Boolean findByObject(@PathVariable("id") String id) {
		return facultyServices.checkFaculty(id);
	}
	//Search 
	@GetMapping("/search/{keyword}")
	public List<Faculty> searchByKeyword(@PathVariable("keyword") String keyword){
		return facultyServices.FacultySearch(keyword);
	}
}
