package university.server.Services;

import java.util.List;
import java.util.Optional;

import university.server.Model.Faculty;

public interface FacultyServices {
	
	List<Faculty> findAll();
	
	public void save(Faculty f);
	
	public void remove(String id);
	
	public boolean checkId(String id);
	
	public Optional<Faculty> findById(String id);
	
	boolean checkFaculty(String id);
	
	public String checkNameFaculty(String falcutyName);
	
	public String checkPhoneFaculty(String phone);
	
	public String getMaxId();
	
	public String checkDuplicateNameFaculty(String id, String facultyName);
	
	public String checkDuplicatePhone(String id,String phone);
	
	public List<Faculty> FacultySearch(String keyword);
}
