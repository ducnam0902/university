package university.server.Services;

import java.util.List;

import university.server.Model.Major;

public interface MajorServices {
	List<Major> findAll();
	public void save(Major m);
	public void delete(String id);
	public boolean checkId(String id);
}
