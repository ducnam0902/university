package university.server.ServicesImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import university.server.JPARepository.FacultyRepository;
import university.server.Model.Faculty;
import university.server.Services.FacultyServices;
@Service
public class FacultyServicesImpl implements FacultyServices  {

	@Autowired
	FacultyRepository falRepo;

	@Override
	public List<Faculty> findAll() {
	
		return falRepo.findAll();
	}

	@Override
	public void save(Faculty f) {
		falRepo.save(f);
	}

	@Override
	public void remove(String id) {
		falRepo.deleteById(id);
	}
	@Override
	public boolean checkId(String id) {
		return falRepo.existsById(id);
	}

	@Override
	public boolean checkFaculty(String id) {
		if(falRepo.findById(id) !=null)
			return true;
		else
			return false;
	}

	@Override
	public Optional<Faculty> findById(String id) {
		return falRepo.findById(id);
	}

	@Override
	public String checkNameFaculty(String falcutyName) {
		return falRepo.checkNameFaculty(falcutyName);
	}

	@Override
	public String checkPhoneFaculty(String phone) {
		return falRepo.checkPhoneFaculty(phone);
	}

	@Override
	public String getMaxId() {
		return falRepo.getMaxId();
	}

	@Override
	public String checkDuplicateNameFaculty(String id, String facultyName) {
		return falRepo.checkDuplicateNameFaculty(id, facultyName);
	}

	@Override
	public String checkDuplicatePhone(String id,String phone) {
		return falRepo.checkDuplicatePhone(id,phone);
	}

	@Override
	public List<Faculty> FacultySearch(String keyword) {
		return falRepo.FacultySearch(keyword);
	}

}
