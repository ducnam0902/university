package university.server.ServicesImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import university.server.JPARepository.MajorRepository;
import university.server.Model.Major;
import university.server.Services.MajorServices;

public class MajorServicesImpl implements MajorServices{

	@Autowired
	MajorRepository majorRepo;
	@Override
	public List<Major> findAll() {
	
		return majorRepo.findAll();
	}

	@Override
	public void save(Major m) {
		majorRepo.save(m);
		
	}

	@Override
	public void delete(String id) {
		majorRepo.deleteById(id);
		
	}

	@Override
	public boolean checkId(String id) {
	
		return majorRepo.existsById(id);
	}

}
